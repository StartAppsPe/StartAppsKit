# StartAppsKit

[![CI Status](http://img.shields.io/travis/Gabriel Lanata/StartAppsKit.svg?style=flat)](https://travis-ci.org/Gabriel Lanata/StartAppsKit)
[![Version](https://img.shields.io/cocoapods/v/StartAppsKit.svg?style=flat)](http://cocoapods.org/pods/StartAppsKit)
[![License](https://img.shields.io/cocoapods/l/StartAppsKit.svg?style=flat)](http://cocoapods.org/pods/StartAppsKit)
[![Platform](https://img.shields.io/cocoapods/p/StartAppsKit.svg?style=flat)](http://cocoapods.org/pods/StartAppsKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

StartAppsKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "StartAppsKit"
```

## Author

Gabriel Lanata, gabriellanata@gmail.com

## License

StartAppsKit is available under the MIT license. See the LICENSE file for more info.



## cd /Users/Gabriel/Dropbox/Projects/StartAppsKit 
## pod lib lint --verbose

## cd example
